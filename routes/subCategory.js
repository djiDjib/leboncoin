const router = require('express').Router();

module.exports = (app) => {
    // Create sous routers SubCategory
    router.post('/create', app.controllers.subCategory.create);
    router.post('/update', app.controllers.subCategory.update);
    router.get('/list', app.controllers.subCategory.list);
    router.post('/remove', app.controllers.subCategory.remove);
    return router;
};
