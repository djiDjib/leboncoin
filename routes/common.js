const router = require('express').Router();

module.exports = (app) => {
    router.post('/login', app.controllers.common.login)
    return router;
};
