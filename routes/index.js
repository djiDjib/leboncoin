module.exports = (app) => {

    // Main routers
    app.use('/common', require('./common')(app));
    app.use('/users', require('./user')(app));
    app.use('/category', require('./category')(app));
    app.use('/products', require('./product')(app));
    app.use('/sous_category', require('./subCategory')(app));
    app.use('/advert', require('./advert')(app))
};
