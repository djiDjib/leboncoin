const router = require('express').Router();

module.exports = (app) => {
    // Create user
    router.post('/create', app.controllers.advert.create);
    router.get('/list', app.controllers.advert.list);
    router.post('/update', app.controllers.advert.update);
    return router;
};
