const router = require('express').Router();

module.exports = (app) => {

    // Create under routers user
    router.post('/create', app.controllers.category.create);
    router.post('/update', app.controllers.category.update);
    router.get('/list', app.controllers.category.list);
    router.post('/remove', app.controllers.category.remove);

    return router;
};
