const router = require('express').Router();

module.exports = (app) => {
    // Create sous routers products
    router.post('/create', app.controllers.product.create);
    router.post('/update', app.controllers.product.update);
    router.get('/list', app.controllers.product.list);
    router.post('/remove', app.controllers.product.remove);
    return router;
};
