const router = require('express').Router();

module.exports = (app) => {
    // Create under routers user
    router.post('/create', app.controllers.user.create);
    router.post('/update', app.controllers.user.update);
    router.post('/remove', app.controllers.user.remove);
    router.get('/list', app.controllers.user.list);
    return router;
};
