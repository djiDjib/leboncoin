const express = require('express')
const app = express()

// Middleware for parsing json
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))

require('./models')(app); console.log('loading models...');
require('./controllers')(app); console.log('loading controllers...');
require('./routes')(app); console.log('loading routes...');

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})