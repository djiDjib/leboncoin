// include lib mongoose
var mongoose = require('mongoose');
// declare schema
var Schema = mongoose.Schema;

// Schema User
var userSchema = new Schema ({
    firstName: String,
    lastName: String,
    birthDate: Date,
    gender: String,
    email: String,
    password: String,
    phone: String,
    isAdmin: {type: Boolean, default: false},
    favorites: [{
        type: Schema.Types.ObjectId,
        ref: 'Advert'
    }],
    registerDate: {type: Date, default: new Date}
})

module.exports = mongoose.model('User', userSchema);
