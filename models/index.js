var mongoose = require('mongoose');

// Correct warning for findAndUpdate function
mongoose.set('useFindAndModify', false);

// Connect to database
mongoose.connect('mongodb://localhost/', {dbName:'leboncoin', useNewUrlParser: true, useUnifiedTopology: true});
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log('Connected to database');
    // Drop the DB
    //mongoose.connection.db.dropDatabase();
});


// Exports Models
module.exports = (app) => {
    app.models = {
        Advert: require('./Advert'),
        Category: require('./Category'),
        SubCategory: require('./SubCategory'),
        Product: require('./Product'),
        User: require('./User')
    }
};
