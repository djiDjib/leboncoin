module.exports = (app) => {

    // Import user's model
    const Product = app.models.Product;

    return { create, update, list, remove };

    // List all Category
    function list(req, res) {
        listProduct()
            .catch(err => console.log(err))
        function listProduct() {
            return new Promise((resolve, reject) => {
                Product.find({}, function (err, item) {
                    if(err) { res.sendStatus(500); reject(err);};
                    if(item) { res.status(200).send(item); resolve()};
                });
            });
        }
    }

    // Create and save user
    function create(req, res) {

        createProduct();

        function createProduct() {
            return new Promise((resolve, reject) => {
                var newProduct = new Product(req.body);
                newProduct.save(function (err, item) {
                    if(err) { reject(err); res.sendStatus(500); };
                    if(item) { resolve(); res.sendStatus(200); };
                });
            });
        }
    }

    // Update category
    function update(req, res) {

        updateProduct()
            .catch(err => console.log(err))

        function updateProduct() {
            return new Promise((resolve, reject) => {
                Product.updateOne({title: req.body.title}, req.body, function (err, item) {
                    if (err) {
                        reject(err);
                        res.sendStatus(500);
                    } else {
                        if (item) {
                            resolve();
                            res.sendStatus(200)
                        } else {
                            reject(500);
                            res.status(500).send('Cannot update user.');
                        }
                        ;
                    }
                })
            });
        }
    }


    // Remove user
    function remove(req, res) {

        removeProduct()
            .catch(err => console.log(err))

        function removeProduct() {
            return new Promise((resolve, reject) => {
                Product.deleteOne({title: req.body.title}, function(err, item) {
                    if(err) { reject(err); res.sendStatus(500); }
                    else { if(item) { resolve(); res.sendStatus(200)} else {reject(500); res.status(500).send('Cannot remove user.');};}
                })
            });
        }
    }


};
