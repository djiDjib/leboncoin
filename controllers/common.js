// Librarie
var jwt = require('jsonwebtoken');
const encryptPassword = require('encrypt-password');

encryptPassword.secret = 'Secrect key never changed.'
encryptPassword.min = 8
encryptPassword.max = 24
encryptPassword.pattern = /^\w{8,24}$/

module.exports = (app) => {

    // Import user's model
    const User = app.models.User;

    return {login};

    // Login and generate jwt token
    function login (req, res) {
        checkEmail()
            .then(foundUser => checkPassword(foundUser))
            .then(foundUser =>generateToken(foundUser))
            .catch(err => console.log(err));

        // Ensure profile exist
        function checkEmail() {
            return new Promise((resolve, reject) => {
                User.findOne({email: req.body.email}, function (err, user) {
                    if (err) {
                        reject(err);res.sendStatus(500);
                    }
                    else {
                        if(user) {
                            //console.log(user);
                            resolve( res.sendStatus(200));
                        }
                        else {
                            res.sendStatus(404);
                        }
                    }
                });
            })
        }

        // Hash password
        function checkPassword(foundUser){
            return new Promise((resolve,reject)=> {
                if(req.body.password) {
                    if(foundUser.password === encryptPassword(req.body.password)){
                        console.log(user);
                        resolve(foundUser);

                    } else {
                        res.status(401).send('Wrong Mail or Pass');
                    }
                }
                else {
                    res.status(401).send('Enter a PASSWORD')
                }
            });
        }

        // token generate
        function generateToken(foundUser){
            return new Promise((resolve,reject)=>{
                let token = jwt.sign({email : foundUser.email},'supersecret');
                if(token){resolve(token); console.log(token); res.status(200).send('TOKEN has been generated')}
                else {res.status(500).send('TOKEN couldn\'t eb genrated')};
            });
        }

    }
}
