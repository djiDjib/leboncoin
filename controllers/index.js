module.exports = (app) => {
    // Exports routers
    app.controllers = {
        user: require('./user')(app),
        category: require('./category')(app),
        product: require('./product')(app),
        subCategory: require('./subCategory')(app),
        advert: require('./advert')(app),
        common: require('./common')(app)
    };
};
