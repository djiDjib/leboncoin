module.exports = (app) => {

    // Import user's model
    const Category = app.models.Category;

    return { create, update, list, remove};

    // Create and save user
    function create(req, res) {
        createCategory();
        function createCategory() {
            return new Promise((resolve, reject) => {
                var newCategory = new Category(req.body);
                newCategory.save(function (err, item) {
                    if(err) { reject(err); res.sendStatus(500); };
                    if(item) { resolve(); res.sendStatus(200); };
                });
            });
        }
    }

    // Update category
    function update(req, res) {
        updateCategory()
            .catch(err => console.log(err))
        function updateCategory() {
            return new Promise((resolve, reject) => {
                Category.updateOne({title: req.body.title}, req.body, function (err, item) {
                    if (err) {
                        reject(err);
                        res.sendStatus(500);
                    } else {
                        if (item) {
                            resolve();
                            res.sendStatus(200)
                        } else {
                            reject(500);
                            res.status(500).send('Cannot update user.');
                        }
                        ;
                    }
                })
            });
        }
    }



    // List all Category
    function list(req, res) {
        listCategory()
            .catch(err => console.log(err))
        function listCategory() {
            return new Promise((resolve, reject) => {
                Category.find({}, function (err, item) {
                    if(err) { res.sendStatus(500); reject(err);};
                    if(item) { res.status(200).send(item); resolve()};
                });
            });
        }
    }

    // Remove user
    function remove(req, res) {
        removeCategory()
            .catch(err => console.log(err))
        function removeCategory() {
            return new Promise((resolve, reject) => {
                Category.deleteOne({title: req.body.title}, function(err, item) {
                    if(err) { reject(err); res.sendStatus(500); }
                    else { if(item) { resolve(); res.sendStatus(200)} else {reject(500); res.status(500).send('Cannot remove user.');};}
                })
            });
        }
    }

};
