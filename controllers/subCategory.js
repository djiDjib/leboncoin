module.exports = (app) => {

    // Import user's model
    const SubCategory = app.models.SubCategory;

    return { create, update, list, remove };

    // List all Category
    function list(req, res) {
        listSubCategory()
            .catch(err => console.log(err))
        function listSubCategory() {
            return new Promise((resolve, reject) => {
                SubCategory.find({}, function (err, item) {
                    if(err) { res.sendStatus(500); reject(err);};
                    if(item) { res.status(200).send(item); resolve()};
                });
            });
        }
    }

    // Create and save user
    function create(req, res) {
        createSubCategory();
        function createSubCategory() {
            return new Promise((resolve, reject) => {
                var newSubCategory= new SubCategory(req.body);
                newSubCategory.save(function (err, item) {
                    if(err) { reject(err); res.sendStatus(500); };
                    if(item) { resolve(); res.sendStatus(200); };
                });
            });
        }
    }


    // Update SubCategory
    function update(req, res) {
        updateSubCategory()
            .catch(err => console.log(err))
        function updateSubCategory() {
            return new Promise((resolve, reject) => {
                SubCategory.updateOne({title: req.body.title}, req.body, function (err, item) {
                    if (err) {
                        reject(err);
                        res.sendStatus(500);
                    } else {
                        if (item) {
                            resolve();
                            res.sendStatus(200)
                        } else {
                            reject(500);
                            res.status(500).send('Cannot update user.');
                        }
                        ;
                    }
                })
            });
        }
    }


    // Remove user
    function remove(req, res) {
        removeSubCategory()
            .catch(err => console.log(err))
        function removeSubCategory() {
            return new Promise((resolve, reject) => {
                SubCategory.deleteOne({title: req.body.title}, function(err, item) {
                    if(err) { reject(err); res.sendStatus(500); }
                    else { if(item) { resolve(); res.sendStatus(200)} else {reject(500); res.status(500).send('Cannot remove user.');};}
                })
            });
        }
    }

 };
