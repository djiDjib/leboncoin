const encryptPassword = require('encrypt-password');
encryptPassword.secret = 'Secrect key never changed.'
encryptPassword.min = 8
encryptPassword.max = 24
encryptPassword.pattern = /^\w{8,24}$/



module.exports = (app) => {

    // Import user's model
    const User = app.models.User;

    return { create, update, remove, list };

    // Create and save user
    function create(req, res) {
        createUser()
        function createUser() {
            return new Promise((resolve, reject) => {
                var newUser = new User(req.body);
                newUser.password = encryptPassword(newUser.password, 'signatrue');
                newUser.save(function (err, item) {
                    if(err) { reject(err); res.sendStatus(500); };
                    if(item) { resolve(); res.sendStatus(200); };
                });
            });
        }
    }


    // Update user
    function update(req, res) {
        updateUser()
            .catch(err => console.log(err))
        function updateUser() {
            return new Promise((resolve, reject) => {
                User.updateOne({email: req.body.email}, req.body, function(err, item) {
                    if(err) { reject(err); res.sendStatus(500); }
                    else { if(item) { resolve(); res.sendStatus(200)} else {reject(500); res.status(500).send('Cannot update user.');};}
                })
            });
        }
    }

    // Remove user
    function remove(req, res) {
        removeUser()
            .catch(err => console.log(err))
        function removeUser() {
            return new Promise((resolve, reject) => {
                User.deleteOne({email: req.body.email}, function(err, item) {
                    if(err) { reject(err); res.sendStatus(500); }
                    else { if(item) { resolve(); res.sendStatus(200)} else {reject(500); res.status(500).send('Cannot remove user.');};}
                })
            });
        }
    }

    // List all users
    function list(req, res) {
        listUsers()
            .catch(err => console.log(err));
        function listUsers() {
            return new Promise((resolve, reject) => {
                User.find({}, function (err, item) {
                    if(err) { res.sendStatus(500); reject(err);};
                    if(item) { res.status(200).send(item); resolve()};
                });
            });
        }
    }
 };
